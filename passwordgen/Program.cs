using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using System.Timers;
using System.Threading.Tasks;

namespace passwordgen
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//	Obtaining key from console user
			string seedKey = null;
			try
			{
				seedKey = args [0];
			}
			//if obtaining key fails assume incorrect syntax
			catch
			{
				Console.WriteLine("Incorrent Arguments");
				Console.WriteLine("Please enter a seed to generate passwords");
				Environment.Exit(1);
			}
			// help args
			if(args[0] == "-help")
			{
				Console.WriteLine("If you have not generated the config file run 'sudo PPG -genconf'");
				Console.WriteLine("Config file located at /etc/PPG/PPG.conf");
				Console.WriteLine("in order to use PPG you must configure the keys you would like it to generate");
			}

			// config gen
			if(args[0] == "-genconf")
			{
				
			}

			//	Number of keys to generate
			List<string> accounts = readConfig();//new List<string> ();
			Console.Clear ();
			Console.WriteLine ();
			Console.WriteLine ("Procedural Password Generator 1.1.2");  
			Console.WriteLine ("Viceroy Systems 2015");
			Console.WriteLine ();
			Console.WriteLine ("Original key = " + seedKey);
			Console.WriteLine ();
			Console.WriteLine ();

			// Generating encrypted keys
			for (int i = 0; i < accounts.Count(); i++) 
			{
				string containerKey;
				containerKey = CreateMD5 (seedKey + accounts[i]);
				containerKey = Base64Encode (containerKey);
				Console.WriteLine (" " + containerKey + " - " + accounts[i]);
			}

			Console.WriteLine ();
		}

		// Function to read the config file from /etc/dopefish/dopefish.conf
		public static List<string> readConfig()
		{
			// creating variables
			List<string> config = new List<string> ();
			List<string> accounts = new List<string>();
			StreamReader dope = new StreamReader("/etc/PPG/PPG.conf");
			string readLine;
			// reading lines to List<string>
			while((readLine = dope.ReadLine()) != null)
			{
				if(readLine.Substring(0,1) != "#")
				{
					config.Add(readLine);
				}
			}
			dope.Close();
			// Parsing information
			foreach(string newLine in config)
			{
				foreach(string line in newLine.Split(','))
				{
					accounts.Add (line);
				}
			}
			return accounts;
		}

		// Function to generate MD5 hash from strings
		public static string CreateMD5(string input)
		{
			// Use imput string to calculate MD5 hash
			MD5 md5 = System.Security.Cryptography.MD5.Create ();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes (input);
			byte[] hashBytes = md5.ComputeHash (inputBytes);

			// Convert the byte array to hexadecimal string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hashBytes.Length; i++) 
			{
				sb.Append (hashBytes [i].ToString ("X2"));
			}
			return sb.ToString ();
		}
			

		// Function to generate Base64 hash from strings
		public static string Base64Encode(string plainText)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes (plainText);
			return System.Convert.ToBase64String (plainTextBytes);
		}
	}
}
